# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :ahaton,
  ecto_repos: [Ahaton.Repo]

# Configures the endpoint
config :ahaton, Ahaton.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "pETE0QYM8YsSXuZknxA0FX0wExIzrxyaj0zU2Aj9fvDEA41/1ULf7/8tQQtEkLp8",
  render_errors: [view: Ahaton.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Ahaton.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
