defmodule Ahaton.Repo.Migrations.CreateAutor do
  use Ecto.Migration

  def change do
    create table(:autors) do
      add :name, :string, size: 100
      add :bio, :text

      timestamps()
    end

  end
end
