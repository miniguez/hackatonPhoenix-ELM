defmodule Ahaton.AutorTest do
  use Ahaton.ModelCase

  alias Ahaton.Autor

  @valid_attrs %{bio: "some content", name: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Autor.changeset(%Autor{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Autor.changeset(%Autor{}, @invalid_attrs)
    refute changeset.valid?
  end
end
