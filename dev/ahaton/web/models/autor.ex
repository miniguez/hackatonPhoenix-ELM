defmodule Ahaton.Autor do
  use Ahaton.Web, :model

  schema "autors" do
    field :name, :string
    field :bio, :string

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :bio])
    |> validate_required([:name, :bio])
  end
end
